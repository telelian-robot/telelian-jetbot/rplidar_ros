import os

import yaml

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import LogInfo
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():
    pkg_dir = get_package_share_directory('rplidar_ros')
    tf_path = f'{pkg_dir}/config/tf_jetbot_ros_ai.config.yaml'
    config_path = f'{pkg_dir}/config/rplidar_a1_jetbot_ros_ai.config.yaml'
    namespace = LaunchConfiguration('namespace')
    
    with open(tf_path, 'r') as f:
        tf_config = yaml.load(f, Loader=yaml.FullLoader)
    
    return LaunchDescription([
        DeclareLaunchArgument(
            "namespace",
            default_value='jetbot_ros_ai',
        ),
        Node(
            package='rplidar_ros',
            executable='rplidar_node',
            name='rplidar_node',
            namespace=namespace,
            parameters=[config_path],
            output='screen'),
        
        Node(
            package='tf2_ros',
            executable='static_transform_publisher',
            name='static_transform_publisher_rplidar_ros',
            output='screen',
            arguments=[
                "--x", tf_config['translation']['x'],
                "--y", tf_config['translation']['y'],
                "--z", tf_config['translation']['z'],
                "--roll", tf_config['rpy']['roll'],
                "--pitch", tf_config['rpy']['pitch'],
                "--yaw", tf_config['rpy']['yaw'],
                "--frame-id", tf_config['frame_id'],
                "--child-frame-id", tf_config['child_frame_id'],
            ],
        ),
    ])

